﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app
{
    internal class Notebook
    {
        private readonly Dictionary<string, int> _contactsStartWithStr = new Dictionary<string, int>();

        public void AddContact(string contact)
        {
            string startString = string.Empty;
            foreach (char letter in contact)
            {
                startString += letter;
                if (_contactsStartWithStr.ContainsKey(startString))
                {
                    _contactsStartWithStr[startString]++;
                }
                else
                {
                    _contactsStartWithStr[startString] = 1;
                }
            }
        }

        public int GetContactsNumberStartWithString(string search)
        {
            int number = 0;
            if (_contactsStartWithStr.ContainsKey(search))
            {
                number = _contactsStartWithStr[search];
            }

            return number;
        }
    }
}