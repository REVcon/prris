﻿using System;
using System.Collections.Generic;

namespace app
{
    internal class Program
    {
        static void Main()
        {
            Console.WriteLine("Notebook");
            Console.WriteLine("Commands:");
            Console.WriteLine("Add <name>");
            Console.WriteLine("Find <search string>");
            Console.WriteLine("Exit");
            Console.WriteLine();

            Notebook notebook = new Notebook();
            while (true)
            {
                Console.WriteLine("Enter command:");
                string input = Console.ReadLine();
                var args = input.Split();
                string action = args[0];
                switch (action)
                {
                    case "Add":
                        if (args.Length == 2)
                        {
                            notebook.AddContact(args[1]);
                        }
                        else
                        {
                            Console.WriteLine("Invalid args");
                        }

                        break;
                    case "Find":
                        if (args.Length == 2)
                        {
                            int number = notebook.GetContactsNumberStartWithString(args[1]);
                            Console.WriteLine($"Find {args[1]} -> {number}");
                            notebook.AddContact(args[1]);
                        }
                        else
                        {
                            Console.WriteLine("Invalid args");
                        }

                        break;
                    case "Exit":
                        return;
                    default:
                        Console.WriteLine("Invalid command");
                        break;
                }
            }
        }
    }
}