﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace app
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine(
                    "Program calculate distance between two words, expected <input file> <first word> <second word>");
            }

            string filename = args[0];
            string firstWord = args[1];
            string secondWord = args[2];
            string[] lines = File.ReadLines(filename).ToArray();

            DistanceCalculator.Calculate(lines, firstWord, secondWord);
        }
    }
}