﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace app
{
    class DistanceCalculator
    {
        internal static void Calculate(
            string[] lines,
            string firstWord,
            string secondWord)
        {
            int index = 0;
            List<int> firstWordIndexes = new List<int>();
            List<int> secondWordIndexes = new List<int>();

            foreach (string line in lines)
            {
                var punctuation = line.Where(char.IsPunctuation).Distinct().ToArray();
                var words = line.Split().Select(x => x.Trim(punctuation));

                foreach (string word in words)
                {
                    index++;
                    if (word == firstWord)
                    {
                        firstWordIndexes.Add(index);
                    }
                    else if (word == secondWord)
                    {
                        secondWordIndexes.Add(index);
                    }
                }
            }

            List<Tuple<int, int>> pairs = new List<Tuple<int, int>>();
            foreach (int firstWordIndex in firstWordIndexes)
            {
                foreach (int secondWordIndex in secondWordIndexes)
                {
                    pairs.Add(new Tuple<int, int>(firstWordIndex, secondWordIndex));
                }
            }

            List<int> distances = new List<int>();
            foreach (Tuple<int, int> pair in pairs)
            {
                int distance = Math.Abs(pair.Item1 - pair.Item2) - 1;
                distances.Add(distance);
            }

            distances.Sort();

            if (distances.Count > 0)
            {
                Console.WriteLine($"min distance: {distances[0]}");
                Console.WriteLine($"max distance: {distances[distances.Count - 1]}");
            }
            else
            {
                Console.WriteLine("No result: words or one of words were not found in text");
            }
        }
    }
}