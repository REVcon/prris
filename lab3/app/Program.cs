﻿using System;
using System.Collections.Generic;
using System.Linq;
using lib;
using lib.Product;

namespace app
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IProduct> products = new List<IProduct>
            {
                new Product("A", 100),
                new Product("B", 250),
                new Product("I", 300),
                new Product("H", 410),
                new Product("E", 500),
            };

            var discountCalculator = new DiscountCalculator();
            var discount = discountCalculator.CalculateDiscount(products);
            var price = products.Aggregate(0.0f, (i, x) => i + x.Price);
            var totalPrice = price - discount;
            Console.WriteLine($"Discount: {discount}");
            Console.WriteLine($"Price without discount: {price}");
            Console.WriteLine($"Price with discount: {totalPrice}");
        }
    }
}