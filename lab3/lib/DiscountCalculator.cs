﻿using System.Collections.Generic;
using System.Linq;
using lib.Discount;
using lib.Product;

namespace lib
{
    public class DiscountCalculator
    {
        private readonly List<IDiscount> _discounts = new List<IDiscount>
        {
            new Discount1(),
            new Discount2(),
            new Discount3(),
            new Discount4(),
            new Discount5(),
            new Discount6(),
            new Discount7()
        };

        public float CalculateDiscount(List<IProduct> products)
        {
            foreach (IDiscount discount in _discounts)
            {
                var productsWithoutDiscount = products.Where(x => !x.WithDiscount()).ToList();
                discount.ApplyDiscount(productsWithoutDiscount);
            }

            return products.Aggregate(0.0f, (i, x) => i + x.GetDiscountAmount());
        }
    }
}