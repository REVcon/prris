﻿using lib.Discount;

namespace lib.Product
{
    public interface IProduct
    {
        string Type { get; }
        float Price { get; }

        void AttachDiscount(IDiscount discount);
        float GetDiscountAmount();
        bool WithDiscount();
    }
}