﻿using lib.Discount;

namespace lib.Product
{
    public class Product : IProduct
    {
        private IDiscount _discount;

        public Product(string type, float price)
        {
            Type = type;
            Price = price;
        }

        public string Type { get; }
        public float Price { get; }

        public void AttachDiscount(IDiscount discount)
        {
            _discount = discount;
        }

        public float GetDiscountAmount()
        {
            if (_discount == null)
            {
                return 0;
            }

            return _discount.GetDiscountAmountForProduct(this);
        }

        public bool WithDiscount()
        {
            return _discount != null;
        }
    }
}