﻿using System.Collections.Generic;
using System.Linq;
using lib.Product;

namespace lib.Discount
{
    internal class Discount1 : IDiscount
    {
        public int DiscountValue => 10;

        public void ApplyDiscount(List<IProduct> products)
        {
            var aTypeProducts = products.Where(x => x.Type == "A").ToArray();
            var bTypeProducts = products.Where(x => x.Type == "B").ToArray();

            int minLen = aTypeProducts.Length > bTypeProducts.Length ? bTypeProducts.Length : aTypeProducts.Length;
            for (int i = 0; i < minLen; i++)
            {
                aTypeProducts[i].AttachDiscount(this);
                bTypeProducts[i].AttachDiscount(this);
            }
        }

        public float GetDiscountAmountForProduct(IProduct product)
        {
            return product.Price * DiscountValue / 100;
        }
    }
}