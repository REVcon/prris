﻿using System.Collections.Generic;
using System.Linq;
using lib.Product;

namespace lib.Discount
{
    internal class Discount4 : IDiscount
    {
        public int DiscountValue => 5;

        public void ApplyDiscount(List<IProduct> products)
        {
            var availableProducts = new List<string> {"K", "L", "M"};
            var aTypeProducts = products.Where(x => x.Type == "A").ToArray();
            var klmTypeProducts = products.Where(x => availableProducts.Contains(x.Type)).ToArray();

            int minLen = aTypeProducts.Length > klmTypeProducts.Length ? klmTypeProducts.Length : aTypeProducts.Length;
            for (int i = 0; i < minLen; i++)
            {
                aTypeProducts[i].AttachDiscount(this);
                klmTypeProducts[i].AttachDiscount(this);
            }
        }

        public float GetDiscountAmountForProduct(IProduct product)
        {
            if (product.Type == "A")
            {
                return 0;
            }

            return product.Price * DiscountValue / 100;
        }
    }
}