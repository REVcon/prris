﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lib.Product;

namespace lib.Discount
{
    internal class Discount2: IDiscount
    {
        public int DiscountValue => 5;
        public void ApplyDiscount(List<IProduct> products)
        {
            var dTypeProducts = products.Where(x => x.Type == "D").ToArray();
            var eTypeProducts = products.Where(x => x.Type == "E").ToArray();

            int minLen = dTypeProducts.Length > eTypeProducts.Length ? eTypeProducts.Length : dTypeProducts.Length;
            for (int i = 0; i < minLen; i++)
            {
                dTypeProducts[i].AttachDiscount(this);
                eTypeProducts[i].AttachDiscount(this);
            }
        }

        public float GetDiscountAmountForProduct(IProduct product)
        {
            return product.Price * DiscountValue / 100;
        }
    }
}
