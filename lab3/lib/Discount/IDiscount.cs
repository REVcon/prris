﻿using System.Collections.Generic;
using lib.Product;

namespace lib.Discount
{
    public interface IDiscount
    {
        int DiscountValue { get; }

        void ApplyDiscount(List<IProduct> products);

        float GetDiscountAmountForProduct(IProduct product);
    }
}