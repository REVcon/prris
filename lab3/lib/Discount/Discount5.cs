﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lib.Product;

namespace lib.Discount
{
    internal class Discount5: IDiscount
    {
        public int DiscountValue => 5;

        public void ApplyDiscount(List<IProduct> products)
        {
            var notAcProducts = products.Where(x => x.Type != "A" && x.Type != "C").ToList();
            if (notAcProducts.Count < 3)
            {
                return;
            }
            for (int i = 0; i < 3; i++)
            {
                notAcProducts[i].AttachDiscount(this);
            }
        }

        public float GetDiscountAmountForProduct(IProduct product)
        {
            return product.Price * DiscountValue / 100;
        }
    }
}
