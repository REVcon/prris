﻿using System.Collections.Generic;
using System.Linq;
using lib.Product;

namespace lib.Discount
{
    internal class Discount7: IDiscount
    {
        public int DiscountValue => 20;

        public void ApplyDiscount(List<IProduct> products)
        {
            var notAcProducts = products.Where(x => x.Type != "A" && x.Type != "C").ToList();
            if (notAcProducts.Count < 5)
            {
                return;
            }
            for (int i = 0; i < 5; i++)
            {
                notAcProducts[i].AttachDiscount(this);
            }
        }

        public float GetDiscountAmountForProduct(IProduct product)
        {
            return product.Price * DiscountValue / 100;
        }
    }
}
