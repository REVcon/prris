﻿using lib.Product;
using System.Collections.Generic;
using System.Linq;

namespace lib.Discount
{
    internal class Discount3 : IDiscount
    {
        public int DiscountValue => 5;

        public void ApplyDiscount(List<IProduct> products)
        {
            var eTypeProducts = products.Where(x => x.Type == "E").ToArray();
            var fTypeProducts = products.Where(x => x.Type == "F").ToArray();
            var gTypeProducts = products.Where(x => x.Type == "G").ToArray();

            var lens = new List<int> {eTypeProducts.Length, fTypeProducts.Length, gTypeProducts.Length};
            lens.Sort();

            int minLen = lens[0];
            for (int i = 0; i < minLen; i++)
            {
                eTypeProducts[i].AttachDiscount(this);
                fTypeProducts[i].AttachDiscount(this);
                gTypeProducts[i].AttachDiscount(this);
            }
        }

        public float GetDiscountAmountForProduct(IProduct product)
        {
            return product.Price * DiscountValue / 100;
        }
    }
}